import be.foreach.bitbucket.hook.GitStringOutputHandler;
import be.foreach.bitbucket.hook.NotificationSettings;
import be.foreach.bitbucket.hook.PushEmailNotifier;
import be.foreach.bitbucket.hook.SmtpNotificationRenderer;
import com.atlassian.bitbucket.auth.AuthenticationContext;
import com.atlassian.bitbucket.commit.Changeset;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.commit.MinimalCommit;
import com.atlassian.bitbucket.content.Change;
import com.atlassian.bitbucket.content.ChangeType;
import com.atlassian.bitbucket.content.Path;
import com.atlassian.bitbucket.hook.repository.RepositoryHookContext;
import com.atlassian.bitbucket.i18n.KeyedMessage;
import com.atlassian.bitbucket.mail.MailMessage;
import com.atlassian.bitbucket.mail.MailService;
import com.atlassian.bitbucket.mail.MailSizeExceededException;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.repository.MinimalRef;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.StandardRefType;
import com.atlassian.bitbucket.scm.git.command.GitCommand;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.scm.git.command.GitScmCommandBuilder;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.bitbucket.user.Person;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageImpl;
import com.atlassian.bitbucket.util.PageRequestImpl;
import com.atlassian.templaterenderer.RenderingException;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.internal.matchers.Equals;
import org.mockito.runners.MockitoJUnitRunner;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestPushEmailNotifier {

    private static final String EMAIL1 = "nobody@there.com";
    private static final String EMAIL2 = "somebody@here.com";

    @Mock
    private CommitService commitService;
    @Mock
    private MailService mailService;
    @Mock
    private NavBuilder navBuilder;
    @Mock
    private GitCommandBuilderFactory gitCommandBuilderFactory;
    @Mock
    private AuthenticationContext authenticationContext;
    @Mock
    private ApplicationPropertiesService applicationPropertiesService;
    private TemplateRenderer templateRenderer;

    private static String crTolerantEq(String expected) {
        return (String) argThat(new CarriageReturnTolerantEquals(expected));
    }

    @Before
    public void setUp() throws Exception {
        // Make sure the initializeParserPool is called to avoid a NPE
        RuntimeSingleton.init();
        templateRenderer = new TemplateRenderer() {
            @Override
            public void render(String templateName, Writer writer) throws RenderingException, IOException {

            }

            @Override
            public void render(String templateName, Map<String, Object> context, Writer writer) throws RenderingException, IOException {

            }

            @Override
            public String renderFragment(String fragment, Map<String, Object> context) throws RenderingException {
                StringWriter stringWriter = new StringWriter();
                VelocityContext velocityContext = new VelocityContext(context);

                try {
                    Velocity.evaluate(velocityContext, stringWriter, "test", fragment);
                } catch (IOException ignore) {

                }

                return stringWriter.toString();
            }

            @Override
            public boolean resolve(String templateName) {
                return false;
            }
        };



        reset(commitService, mailService, navBuilder, gitCommandBuilderFactory, authenticationContext, applicationPropertiesService);
        when(applicationPropertiesService.getServerEmailAddress()).thenReturn("test.foreach.be");
    }

    @Test
    public void testDeletedBranch() throws Exception {
        SmtpNotificationRenderer notificationRenderer = spy(new SmtpNotificationRenderer(mailService, commitService, navBuilder, gitCommandBuilderFactory));
        PushEmailNotifier notifier = new PushEmailNotifier(notificationRenderer, authenticationContext, applicationPropertiesService, gitCommandBuilderFactory, templateRenderer);

        Collection<RefChange> refChanges = new ArrayList<>();
        MinimalRef minimalRef = mock(MinimalRef.class);
        when(minimalRef.getId()).thenReturn("refs/heads/branchname");
        RefChange refChange = mock(RefChange.class);
        when(refChange.getRef()).thenReturn(minimalRef);
        when(refChange.getFromHash()).thenReturn(StringUtils.rightPad("51zefz15", 40, "a"));
        when(refChange.getToHash()).thenReturn("0000000000000000000000000000000000000000");
        refChanges.add(refChange);

        ApplicationUser applicationUser = getApplicationUser("Test User", "test@email.com");
        when(authenticationContext.getCurrentUser()).thenReturn(applicationUser);

        setNumberOfRealCommits("0");
        notifier.postReceive(getRepositoryHookContext(), refChanges);

        String expectedOutput = PushEmailNotifier.readTextFile(TestPushEmailNotifier.class.getClassLoader().getResourceAsStream("test_deleted_branch.html"), "UTF-8");
        verify(notificationRenderer).sendMail(eq(Arrays.asList(EMAIL1)), eq("Repository name <repo_59@test.foreach.be>"), eq("[51zefz1..0000000@refs/heads/branchname] Branch deleted"), crTolerantEq(expectedOutput), eq( Boolean.FALSE ));
    }

    @Test
    public void testCreatedBranch() throws Exception {
        SmtpNotificationRenderer notificationRenderer = spy(new SmtpNotificationRenderer(mailService, commitService, navBuilder, gitCommandBuilderFactory));
        PushEmailNotifier notifier = new PushEmailNotifier(notificationRenderer, authenticationContext, applicationPropertiesService, gitCommandBuilderFactory, templateRenderer);

        Collection<RefChange> refChanges = new ArrayList<>();
        MinimalRef minimalRef = mock(MinimalRef.class);
        when(minimalRef.getId()).thenReturn("refs/heads/branchname");

        RefChange refChange = mock(RefChange.class);
        when(refChange.getRef()).thenReturn(minimalRef);
        when(refChange.getFromHash()).thenReturn("0000000000000000000000000000000000000000");
        when(refChange.getToHash()).thenReturn("51zefz15");
        refChanges.add(refChange);

        ApplicationUser applicationUser = getApplicationUser("Test User", "test@email.com");
        when(authenticationContext.getCurrentUser()).thenReturn(applicationUser);

        setNumberOfRealCommits("0");
        notifier.postReceive(getRepositoryHookContext(), refChanges);
        String expectedOutput = PushEmailNotifier.readTextFile(TestPushEmailNotifier.class.getClassLoader().getResourceAsStream("test_created_branch.html"), "UTF-8");
        verify(notificationRenderer).sendMail(eq(Arrays.asList(EMAIL1)), eq("Repository name <repo_59@test.foreach.be>"), eq("[0000000..51zefz1@refs/heads/branchname] Branch created"), crTolerantEq(expectedOutput), eq( Boolean.FALSE ));
    }

    @Test
    public void testSemicolonSeparatedEmails() throws Exception {
        testEmailAddresses(" " + EMAIL1 + "; " + EMAIL2 + " ", EMAIL1, EMAIL2);
    }

    @Test
    public void testCommaSeparatedEmails() throws Exception {
        testEmailAddresses(" " + EMAIL1 + ", " + EMAIL2 + " ", EMAIL1, EMAIL2);
    }

    private void testEmailAddresses(String configuredEmails, String... actualEmails) throws Exception {
        SmtpNotificationRenderer notificationRenderer = spy(new SmtpNotificationRenderer(mailService, commitService, navBuilder, gitCommandBuilderFactory));
        PushEmailNotifier notifier = new PushEmailNotifier(notificationRenderer, authenticationContext, applicationPropertiesService, gitCommandBuilderFactory, templateRenderer);

        Collection<RefChange> refChanges = new ArrayList<>();
        RefChange refChange = mock(RefChange.class);
        MinimalRef minimalRef = mock(MinimalRef.class);
        when(minimalRef.getId()).thenReturn("refs/heads/branchname");

        when(refChange.getRef()).thenReturn(minimalRef);
        when(refChange.getFromHash()).thenReturn("0000000000000000000000000000000000000000");
        when(refChange.getToHash()).thenReturn("51zefz15");
        refChanges.add(refChange);

        ApplicationUser applicationUser = getApplicationUser("Test User", "test@email.com");
        when(authenticationContext.getCurrentUser()).thenReturn(applicationUser);

        setNumberOfRealCommits("0");
        notifier.postReceive(getRepositoryHookContextForEmails(configuredEmails, false), refChanges);
        String expectedOutput = PushEmailNotifier.readTextFile(TestPushEmailNotifier.class.getClassLoader().getResourceAsStream("test_created_branch.html"), "UTF-8");
        verify(notificationRenderer).sendMail(eq(Arrays.asList(actualEmails)), eq("Repository name <repo_59@test.foreach.be>"), eq("[0000000..51zefz1@refs/heads/branchname] Branch created"), crTolerantEq(expectedOutput), eq( Boolean.FALSE ));
    }

    @Test
    public void testCreatedBranchWithCommits() throws Exception {
        SmtpNotificationRenderer notificationRenderer = spy(new SmtpNotificationRenderer(mailService, commitService, navBuilder, gitCommandBuilderFactory));
        PushEmailNotifier notifier = new PushEmailNotifier(notificationRenderer, authenticationContext, applicationPropertiesService, gitCommandBuilderFactory, templateRenderer);

        Collection<RefChange> refChanges = new ArrayList<>();
        MinimalRef minimalRef = mock(MinimalRef.class);
        when(minimalRef.getId()).thenReturn("refs/heads/branchname");

        RefChange refChange = mock(RefChange.class);
        when(refChange.getRef()).thenReturn(minimalRef);
        when(refChange.getFromHash()).thenReturn("0000000000000000000000000000000000000000");
        when(refChange.getToHash()).thenReturn("51zefz15");
        refChanges.add(refChange);

        ApplicationUser applicationUser = getApplicationUser("Test User", "test@email.com");
        when(authenticationContext.getCurrentUser()).thenReturn(applicationUser);

        RepositoryHookContext repositoryHookContext = getRepositoryHookContext();

        Collection<Commit> commits = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Commit commit = getMockedCommit(i);
            commits.add(commit);

            List<String> branches = new ArrayList<>();
            branches.add("thisbranch");
            if (i == 1 || i == 2) {
                // Commit 1 and 2 exist on another branch already, skip them for new branch
                branches.add("master");
            }
            doReturn(branches).when(notificationRenderer).getBranches(repositoryHookContext.getRepository(), commit);
            doReturn("http://bitbucket.internal/projects/PROJECT/repos/REPO/commits/" + commit.getId()).when(notificationRenderer).getChangesetUrl(repositoryHookContext.getRepository(), commit);

            List<MinimalCommit> parents = new ArrayList<>();
            MinimalCommit parent1 = mock(MinimalCommit.class);
            doReturn("http://bitbucket.internal/projects/PROJECT/repos/REPO/commits/parent1").when(notificationRenderer).getMinimalChangesetUrl(repositoryHookContext.getRepository(), parent1);
            MinimalCommit parent2 = mock(MinimalCommit.class);
            doReturn("http://bitbucket.internal/projects/PROJECT/repos/REPO/commits/parent2").when(notificationRenderer).getMinimalChangesetUrl(repositoryHookContext.getRepository(), parent2);

            when(parent1.getDisplayId()).thenReturn("parent1");
            when(parent2.getDisplayId()).thenReturn("parent2");

            parents.add(parent1);
            parents.add(parent2);

            when(commit.getParents()).thenReturn(parents);
        }

        Page<Commit> changesetPage = new PageImpl<>(new PageRequestImpl(0, 10), commits, false);
        when(commitService.getCommitsBetween(anyObject(), anyObject())).thenReturn(changesetPage);

        Collection<Changeset> detailedChangesets = new ArrayList<>();
        Changeset detailedChangeset = mock(Changeset.class);
        detailedChangesets.add(detailedChangeset);

        Collection<Change> changes = new ArrayList<>();

        for (int i = 0; i < 17; i++) {
            Change change = mock(Change.class);
            Path path = mock(Path.class);
            when(path.toString()).thenReturn("some/src/main/java/file" + i + ".java");
            when(change.getType()).thenReturn(i % 2 == 0 ? ChangeType.ADD : ChangeType.DELETE);
            when(change.getPath()).thenReturn(path);
            changes.add(change);
        }

        Page<? extends Change> changePage = new PageImpl<>(new PageRequestImpl(0, 10), changes, false);

        doReturn(changePage).when(detailedChangeset).getChanges();

        Page<Changeset> detailedChangesetPage = new PageImpl<>(new PageRequestImpl(0, 10), detailedChangesets, false);
        when(commitService.getChangesets(anyObject(), anyObject())).thenReturn(detailedChangesetPage);

        setNumberOfRealCommits("5");
        notifier.postReceive(repositoryHookContext, refChanges);
        String expectedOutput = PushEmailNotifier.readTextFile(TestPushEmailNotifier.class.getClassLoader().getResourceAsStream("test_created_branch_with_commits.html"), "UTF-8");
        verify(notificationRenderer).sendMail(eq(Arrays.asList(EMAIL1)), eq("Repository name <repo_59@test.foreach.be>"), eq("[0000000..51zefz1@refs/heads/branchname] Branch created"), crTolerantEq(expectedOutput), eq( Boolean.FALSE ));
    }

    @Test
    public void testPushWithOneCommit() throws Exception {
        SmtpNotificationRenderer notificationRenderer = spy(new SmtpNotificationRenderer(mailService, commitService, navBuilder, gitCommandBuilderFactory));
        PushEmailNotifier notifier = new PushEmailNotifier(notificationRenderer, authenticationContext, applicationPropertiesService, gitCommandBuilderFactory, templateRenderer);

        Collection<RefChange> refChanges = new ArrayList<>();
        MinimalRef minimalRef = mock(MinimalRef.class);
        when(minimalRef.getId()).thenReturn("refs/heads/branchname");

        RefChange refChange = mock(RefChange.class);
        when(refChange.getRef()).thenReturn(minimalRef);
        when(refChange.getFromHash()).thenReturn(StringUtils.rightPad("889218518", 40, "e"));
        when(refChange.getToHash()).thenReturn(StringUtils.rightPad("9999", 40, "f"));
        refChanges.add(refChange);

        ApplicationUser applicationUser = getApplicationUser("Test User", "test@email.com");
        when(authenticationContext.getCurrentUser()).thenReturn(applicationUser);

        Collection<Commit> commits = new ArrayList<>();

        Commit commit = mock(Commit.class);
        when(commit.getMessage()).thenReturn("Unique      <pre>     message\r\n        with new line\r\n\r\n\r\n      \r\n\r\n   last line    ");
        when(commit.getId()).thenReturn(StringUtils.rightPad("1253456", 40, "0"));
        commits.add(commit);

        Page<Commit> changesetPage = new PageImpl<>(new PageRequestImpl(0, 10), commits, false);
        when(commitService.getCommitsBetween(anyObject(), anyObject())).thenReturn(changesetPage);

        Collection<Changeset> detailedChangesets = new ArrayList<>();
        Changeset detailedChangeset = mock(Changeset.class);
        detailedChangesets.add(detailedChangeset);

        Page<Changeset> detailedChangesetPage = new PageImpl<>(new PageRequestImpl(0, 10), detailedChangesets, false);
        when(commitService.getChangesets(anyObject(), anyObject())).thenReturn(detailedChangesetPage);

        RepositoryHookContext repositoryHookContext = getRepositoryHookContext();
        doReturn("http://bitbucket.internal/projects/PROJECT/repos/REPO/commits/" + commit.getId()).when(notificationRenderer).getChangesetUrl(repositoryHookContext.getRepository(), commit);

        setNumberOfRealCommits("1");
        notifier.postReceive(repositoryHookContext, refChanges);

        String expectedOutput = PushEmailNotifier.readTextFile(TestPushEmailNotifier.class.getClassLoader().getResourceAsStream("test_pushed_branch_1_commit.html"), "UTF-8");
        verify(notificationRenderer).sendMail(eq(Arrays.asList(EMAIL1)), eq("Repository name <repo_59@test.foreach.be>"), eq("[9999fff@refs/heads/branchname] Unique <pre> message with new line last line"), crTolerantEq(expectedOutput), eq( Boolean.FALSE ));
    }

    @Test
    public void TestMailSizeExceededThrowsException() throws IOException {
        SmtpNotificationRenderer notificationRenderer = spy(new SmtpNotificationRenderer(mailService, commitService, navBuilder, gitCommandBuilderFactory));
        PushEmailNotifier notifier = new PushEmailNotifier(notificationRenderer, authenticationContext, applicationPropertiesService, gitCommandBuilderFactory, templateRenderer);

        ApplicationUser applicationUser = getApplicationUser("Test User", "test@email.com");
        when(authenticationContext.getCurrentUser()).thenReturn(applicationUser);

        final String expectedFrom = "Repository name <repo_59@test.foreach.be>";
        final String expectedSubject = "[0000000..9999fff@refs/heads/branchname] Branch created";

        KeyedMessage keyedMessage = new KeyedMessage("key", "localized", "Message size exceed 451651 bytes");
        doThrow(new MailSizeExceededException(keyedMessage)).when(mailService).submit(argThat(new MailMessageBodyContainsArgumentMatcher(expectedFrom, expectedSubject, "<h3 style=\"font-size:10pt; padding:0px; margin:0;\">MODIFIED PATH(S)</h3>")));

        Collection<RefChange> refChanges = new ArrayList<>();
        MinimalRef minimalRef = mock(MinimalRef.class);
        when(minimalRef.getId()).thenReturn("refs/heads/branchname");
        when(minimalRef.getType()).thenReturn(StandardRefType.BRANCH);
        RefChange refChange = mock(RefChange.class);
        when(refChange.getRef()).thenReturn(minimalRef);
        when(refChange.getFromHash()).thenReturn(StringUtils.rightPad("0", 40, "0"));
        when(refChange.getToHash()).thenReturn(StringUtils.rightPad("9999", 40, "f"));
        refChanges.add(refChange);

        Collection<Commit> commits = new ArrayList<>();

        Commit commit = mock(Commit.class);
        when(commit.getMessage()).thenReturn("Test with lots of commits should throw exception");
        when(commit.getId()).thenReturn(StringUtils.rightPad("1253456", 40, "0"));
        commits.add(commit);

        Page<Commit> changesetPage = new PageImpl<>(new PageRequestImpl(0, 10), commits, false);
        when(commitService.getCommitsBetween(anyObject(), anyObject())).thenReturn(changesetPage);

        Collection<Changeset> detailedChangesets = new ArrayList<>();
        Changeset detailedChangeset = mock(Changeset.class);
        detailedChangesets.add(detailedChangeset);

        Page<Changeset> detailedChangesetPage = new PageImpl<>(new PageRequestImpl(0, 10), detailedChangesets, false);
        when(commitService.getChangesets(anyObject(), anyObject())).thenReturn(detailedChangesetPage);

        RepositoryHookContext repositoryHookContext = getRepositoryHookContext();
        doReturn("http://bitbucket.internal/projects/PROJECT/repos/REPO/commits/" + commit.getId()).when(notificationRenderer).getChangesetUrl(repositoryHookContext.getRepository(), commit);

        setNumberOfRealCommits("156161");

        try {
            notifier.postReceive(repositoryHookContext, refChanges);
        } catch (MailSizeExceededException ex) {
            // Ignore exception here
        }

        ArgumentCaptor<String> fromCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> subjectCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> bodyCaptor = ArgumentCaptor.forClass(String.class);
        verify(notificationRenderer, times(2)).sendMail(eq(Arrays.asList(EMAIL1)), fromCaptor.capture(), subjectCaptor.capture(), bodyCaptor.capture(), eq( Boolean.FALSE ));
        List<String> fromNames = fromCaptor.getAllValues();
        List<String> subjects = subjectCaptor.getAllValues();
        List<String> bodies = bodyCaptor.getAllValues();

        for (String fromName : fromNames) {
            assertEquals(expectedFrom, fromName);
        }

        for (String subject : subjects) {
            assertEquals(expectedSubject, subject);
        }

        String expectedOutput = PushEmailNotifier.readTextFile(TestPushEmailNotifier.class.getClassLoader().getResourceAsStream("test_created_branch_too_large.html"), "UTF-8");
        String expectedOutputLightMail = PushEmailNotifier.readTextFile(TestPushEmailNotifier.class.getClassLoader().getResourceAsStream("test_created_branch_too_large_light.html"), "UTF-8");
        assertThat(bodies.get(0), new CarriageReturnTolerantEquals(expectedOutput));
        assertThat(bodies.get(1), new CarriageReturnTolerantEquals(expectedOutputLightMail));
    }

    @Test
    public void testPlainTextEmail() throws Exception {
        SmtpNotificationRenderer notificationRenderer = spy(new SmtpNotificationRenderer(mailService, commitService, navBuilder, gitCommandBuilderFactory));
        PushEmailNotifier notifier = new PushEmailNotifier(notificationRenderer, authenticationContext, applicationPropertiesService, gitCommandBuilderFactory, templateRenderer);

        Collection<RefChange> refChanges = new ArrayList<>();
        MinimalRef minimalRef = mock(MinimalRef.class);
        when(minimalRef.getId()).thenReturn("refs/heads/branchname");

        RefChange refChange = mock(RefChange.class);
        when(refChange.getRef()).thenReturn(minimalRef);
        when(refChange.getFromHash()).thenReturn("0000000000000000000000000000000000000000");
        when(refChange.getToHash()).thenReturn("51zefz15");
        refChanges.add(refChange);

        ApplicationUser applicationUser = getApplicationUser("Test User", "test@email.com");
        when(authenticationContext.getCurrentUser()).thenReturn(applicationUser);

        setNumberOfRealCommits("0");
        notifier.postReceive(getRepositoryHookContextForEmails(EMAIL1, true), refChanges);
        String expectedOutput = PushEmailNotifier.readTextFile(TestPushEmailNotifier.class.getClassLoader().getResourceAsStream("test_created_branch.html"), "UTF-8");
        verify(notificationRenderer).sendMail(eq(Arrays.asList(EMAIL1)), eq("Repository name <repo_59@test.foreach.be>"), eq("[0000000..51zefz1@refs/heads/branchname] Branch created"), crTolerantEq(expectedOutput), eq( Boolean.TRUE ));
    }


    private Commit getMockedCommit(int commitId) {
        String changeSet = StringUtils.leftPad(Integer.toString(commitId), 40, (char) (commitId + 97));
        Commit commit = mock(Commit.class);
        when(commit.getId()).thenReturn(changeSet);
        Person author = new Person() {
            @Override
            public String getEmailAddress() {
                return "testuser@someone.com";
            }

            @Nonnull
            @Override
            public String getName() {
                return "Test User Commit";
            }
        };
        when(commit.getAuthor()).thenReturn(author);
        Calendar cal = Calendar.getInstance();
        cal.set(2010, Calendar.NOVEMBER, 30, 10, 15, 40);
        Date authorTimestamp = cal.getTime();
        when(commit.getAuthorTimestamp()).thenReturn(authorTimestamp);
        when(commit.getMessage()).thenReturn("Commit message for test case " + changeSet + " remove <li> Invalidate edited users &#39; &copy; &#169; sessions from html files");
        return commit;
    }

    private RepositoryHookContext getRepositoryHookContext() {
        return getRepositoryHookContextForEmails(EMAIL1, false);
    }

    private RepositoryHookContext getRepositoryHookContextForEmails(String emailAddresses, boolean plainTextEmail) {
        Repository repository = mock(Repository.class);
        when(repository.getId()).thenReturn(59);
        when(repository.getName()).thenReturn("Repository name");
        Settings settings = mock(Settings.class);
        when(settings.getBoolean(NotificationSettings.FIELD_PLAINTEXT_EMAIL)).thenReturn(plainTextEmail);
        when(settings.getString("mailToAddress")).thenReturn(emailAddresses);
        when(settings.getString("branchBlacklist")).thenReturn("blacklist-me");
        return new RepositoryHookContext(repository, settings);
    }

    private ApplicationUser getApplicationUser(String displayName, String emailAddress) {
        ApplicationUser user = mock(ApplicationUser.class);
        when(user.getDisplayName()).thenReturn(displayName);
        when(user.getEmailAddress()).thenReturn(emailAddress);
        return user;
    }

    private void setNumberOfRealCommits(String numberOfCommits) {
        GitScmCommandBuilder gitScmCommandBuilder = mock(GitScmCommandBuilder.class);
        when(gitScmCommandBuilder.command(anyString())).thenReturn(gitScmCommandBuilder);
        when(gitScmCommandBuilder.argument(anyString())).thenReturn(gitScmCommandBuilder);
        GitCommand gitCommand = mock(GitCommand.class);
        when(gitCommand.call()).thenReturn(numberOfCommits);
        when(gitScmCommandBuilder.build(any(GitStringOutputHandler.class))).thenReturn(gitCommand);

        when(gitCommandBuilderFactory.builder(any(Repository.class))).thenReturn(gitScmCommandBuilder);
    }

    private static class CarriageReturnTolerantEquals extends Equals {
        private static final long serialVersionUID = 1L;

        public CarriageReturnTolerantEquals(String wanted) {
            super(stripCRs(wanted));
        }

        private static String stripCRs(String s) {
            return s == null
                    ? null
                    : s.replaceAll("\r\n", "\n");
        }

        @Override
        public boolean matches(Object actual) {
            return super.matches(stripCRs((String) actual));
        }
    }

    private class MailMessageBodyContainsArgumentMatcher extends ArgumentMatcher<MailMessage> {

        private final String expectedFrom;
        private final String expectedSubject;
        private final String expectedBodyContains;

        private MailMessageBodyContainsArgumentMatcher(String expectedFrom, String excpectedSubject, String expectedBodyContains) {
            this.expectedFrom = expectedFrom;
            this.expectedSubject = excpectedSubject;
            this.expectedBodyContains = expectedBodyContains;
        }

        @Override
        public boolean matches(Object argument) {
            if (argument instanceof MailMessage) {
                MailMessage mailMessage = (MailMessage) argument;
                return expectedFrom != null && expectedSubject != null && expectedFrom.equals(mailMessage.getFrom()) && expectedSubject.equals(mailMessage.getSubject()) && mailMessage.getText().contains(expectedBodyContains);
            } else {
                return false;
            }
        }
    }
}
