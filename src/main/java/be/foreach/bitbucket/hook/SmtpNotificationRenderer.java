package be.foreach.bitbucket.hook;

import com.atlassian.bitbucket.commit.*;
import com.atlassian.bitbucket.content.Change;
import com.atlassian.bitbucket.mail.MailMessage;
import com.atlassian.bitbucket.mail.MailService;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.CommandFailedException;
import com.atlassian.bitbucket.scm.git.command.GitCommand;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequestImpl;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SmtpNotificationRenderer {

    private static final Logger LOG = LoggerFactory.getLogger(SmtpNotificationRenderer.class);
    public static final int MAX_PAGE_REQUEST = 1000000;
    public static final String FRIENDLY_NEWLINE = "__NEWLINE__";
    private static final String BRANCH_CREATION_OR_DELETION = "0000000000000000000000000000000000000000";
    private static final String BRANCH_SEP = " / ";
    private static final String REF_ID_TAG = "refs/tags";
    private final MailService mailService;
    private final CommitService commitService;
    private final NavBuilder navBuilder;
    private final GitCommandBuilderFactory gitCommandBuilderFactory;

    public SmtpNotificationRenderer(final MailService mailService, final CommitService commitService, final NavBuilder navBuilder, final GitCommandBuilderFactory gitCommandBuilderFactory) {
        this.mailService = mailService;
        this.commitService = commitService;
        this.navBuilder = navBuilder;
        this.gitCommandBuilderFactory = gitCommandBuilderFactory;
    }

    public void sendMail(List<String> emailAddresses, String from, String subject, String body, Boolean plainTextEmail) {
        for (String emailAddress : emailAddresses) {
            String contentType;
            if( plainTextEmail == null || plainTextEmail == Boolean.FALSE ) {
                contentType = "text/html; charset=UTF-8";
            } else {
                contentType = "text/plain; charset=UTF-8";
            }
            MailMessage mailMessage = new MailMessage.Builder().to(emailAddress).from(from).subject(subject).text(body).header("Content-type", contentType ).build();
            mailService.submit(mailMessage);
        }
    }

    public Page<Commit> getChangesetPage(Repository repository, RefChange refChange) {
        CommitsBetweenRequest commitsBetweenRequest = new CommitsBetweenRequest.Builder(repository).exclude(refChange.getFromHash(), new String[0]).include(refChange.getToHash(), new String[0]).build();
        int maxChangeSets = MAX_PAGE_REQUEST;
        if (BRANCH_CREATION_OR_DELETION.equals(refChange.getFromHash())) {
            // When creating and pushing a new branch, don't fetch all changes, limit it to somewhat 100
            maxChangeSets = 100;
        }
        return commitService.getCommitsBetween(commitsBetweenRequest, new PageRequestImpl(0, maxChangeSets));
    }

    public List<String> getBranches(final Repository repository, final Commit commit) {
        GitStringOutputHandler stringOutputHandler = new GitStringOutputHandler();
        GitCommand<String> gitCommand = gitCommandBuilderFactory.builder(repository).command("branch").argument("-a").argument("--contains").argument(commit.getId()).build(stringOutputHandler);
        String output = null;

        try {
            output = gitCommand.call();
        } catch (CommandFailedException e) {
            LOG.error( "Error getting branches", e );
        }

        if (output != null) {
            String branches = StringUtils.stripEnd(output.replaceAll("(\\t|\\r?\\n)+", BRANCH_SEP), BRANCH_SEP);
            String[] numOfBranches = branches.split(BRANCH_SEP);
            return Arrays.asList(numOfBranches);
        } else {
            return Collections.emptyList();
        }
    }

    public boolean isCreatedBranch(RefChange refChange) {
        return SmtpNotificationRenderer.BRANCH_CREATION_OR_DELETION.equals(refChange.getFromHash());
    }

    public boolean isDeletedBranch(RefChange refChange) {
        return SmtpNotificationRenderer.BRANCH_CREATION_OR_DELETION.equals(refChange.getToHash());
    }

    public boolean isTag(RefChange refChange) {
        return refChange != null && StringUtils.isNotBlank(refChange.getRef().getId()) && refChange.getRef().getId().startsWith(REF_ID_TAG);
    }

    public String getChangesetUrl(final Repository repository, final String hash) {
        return navBuilder.repo(repository).commit(hash).buildConfigured();
    }

    public String getChangesetUrl(final Repository repository, final RefChange refChange) {
        return getChangesetUrl(repository, refChange.getRef().getId());
    }

    public String getChangesetUrl(final Repository repository, final Commit commit) {
        return getChangesetUrl(repository, commit.getId());
    }

    public String getMinimalChangesetUrl(final Repository repository, final MinimalCommit commit) {
        return getChangesetUrl(repository, commit.getId());
    }

    public Page<Changeset> getDetailedChangesetPage(final Repository repository, final Page<Commit> page, final Commit commit) {
        int maxChangesPerCommit = MAX_PAGE_REQUEST;
        if (page.getSize() > 100) {
            // If this is a rather large merge, limit the number of changes to 11, so we can show a ... after the 10th change item
            maxChangesPerCommit = 11;
        }
        ChangesetsRequest.Builder changesetsRequest = new ChangesetsRequest.Builder(repository).ignoreMissing(true).maxChangesPerCommit(maxChangesPerCommit);
        changesetsRequest.commitId(commit.getId());

        // Make number of change sets configurable
        return commitService.getChangesets(changesetsRequest.build(), new PageRequestImpl(0, MAX_PAGE_REQUEST));
    }

    public String stripWhiteSpacesToNewLines(String text) {
        return text.replaceAll(">\\s*<", ">\r\n<");
    }

    public String jsonFriendlyNewLine(String text) {
        return StringUtils.replace(StringUtils.replaceChars(text, '\r', '\0'), "\n", FRIENDLY_NEWLINE);
    }

    public String unescapeHtml3( String text ) {
        return StringEscapeUtils.unescapeHtml3( text );
    }

    public String unescapeHtml4( String text ) {
        return StringEscapeUtils.unescapeHtml4( text );
    }

    public Page<? extends Change> getChanges(Changeset changeset) {
        return changeset.getChanges();
    }

    public String getShortChangesStats(Changeset changeset) {
        Page<? extends Change> changes = changeset.getChanges();
        if (changes != null) {
            int added = 0, deleted = 0, modified = 0, moved = 0, unknown = 0, copied = 0;

            for (Change change : changes.getValues()) {
                switch (change.getType()) {
                    case ADD:
                        added++;
                        break;
                    case COPY:
                        copied++;
                        break;
                    case DELETE:
                        deleted++;
                        break;
                    case MODIFY:
                        modified++;
                        break;
                    case MOVE:
                        moved++;
                        break;
                    case UNKNOWN:
                        unknown++;
                        break;
                }
            }

            int totalChanges = (added + modified + deleted + moved + copied + unknown);
            String mainStats = String.format("%s change" + (totalChanges > 1 ? "s" : StringUtils.EMPTY) + ": %s+/%s~/%s-", totalChanges, added, modified, deleted);
            mainStats += getAdditionStats(copied, "copied");
            mainStats += getAdditionStats(moved, "moved");
            mainStats += getAdditionStats(unknown, "unknown");

            return mainStats;
        } else {
            return "No changes";
        }


    }

    private String getAdditionStats(int type, String text) {
        if (type > 0) {
            return ", " + text;
        }
        return StringUtils.EMPTY;
    }

    @SuppressWarnings("unused")
    public String getShortHash(String refId) {
        if (refId == null) return "";
        return refId.length() >= 7 ? refId.substring(0, 7) : refId;
    }
}
